import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('TurnSfx')
export class TurnSfx extends BaseAudio {
    constructor() {
        super('TurnSfx', ASSET_KEY.TURN);
    }
}