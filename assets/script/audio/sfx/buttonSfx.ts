import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('ButtonSfx')
export class ButtonSfx extends BaseAudio {
    constructor() {
        super('ButtonSfx', ASSET_KEY.BUTTON_SFX);
    }
}