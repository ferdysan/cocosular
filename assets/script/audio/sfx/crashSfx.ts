import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('CrashSfx')
export class CrashSfx extends BaseAudio {
    constructor() {
        super('CrashSfx', ASSET_KEY.CRASH);
    }
}