import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('BackgroundMusic')
export class BackgroundMusic extends BaseAudio {
    constructor() {
        super('BackgroundMusic', ASSET_KEY.DONKEY_KONG_BG_MUSIC, true, 0.5);
    }
}