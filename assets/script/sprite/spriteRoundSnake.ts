
import { _decorator, tween, Color, Node, Sprite } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('SpriteRoundSnake')
export class SpriteRoundSnake extends BaseSprite {
    constructor(frame: number = 0) {
        super('SpriteRoundSnake', ASSET_KEY.SPRITESHEET_ROUND_SNAKE, frame);
    }

    get FrameKey() {
        return this.frameKey;
    }

    turnIntoBodyEating() {
        this.frameKey = 1;
        this.reload();
    }

    turnIntoTail() {
        this.frameKey = 2;
        this.reload();
    }

    turnIntoBody() {
        this.frameKey = 3;
        this.reload();
    }
}