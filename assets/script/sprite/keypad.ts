
import { _decorator, tween, Color, Node, Sprite } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('Keypad')
export class Keypad extends BaseSprite {
    constructor() {
        super('Keypad', ASSET_KEY.KEYPAD);
    }
}