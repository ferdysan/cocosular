
import { _decorator, tween, Color, Node, Sprite } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('LogoShopeeUlar')
export class LogoShopeeUlar extends BaseSprite {
    constructor() {
        super('LogoShopeeUlar', ASSET_KEY.LOGO_SHOPEE_ULAR);
    }

    reload() {
        this.textureKey = ASSET_KEY.LOGO_SHOPEE_ULAR;
        super.reload();
    }
}