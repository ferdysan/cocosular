
import { _decorator, tween, Color, Node, Sprite, AudioSource, find } from 'cc';
import { ButtonSfx } from '../audio/sfx/buttonSfx';
import { CrashSfx } from '../audio/sfx/crashSfx';
import { EatSfx } from '../audio/sfx/eatSfx';
import { TurnSfx } from '../audio/sfx/turnSfx';
import { BackgroundMusic } from '../audio/soundtrack/backgroundMusic';
import { ASSET_KEY } from '../enum/asset';
import { SOUND_STATE } from '../enum/sound';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('SpriteSound')
export class SpriteSound extends BaseSprite {
    @property(EatSfx)
    public readonly eatSfx?: EatSfx;

    @property(CrashSfx)
    public readonly crashSfx?: CrashSfx;

    @property(TurnSfx)
    public readonly turnSfx?: TurnSfx;

    @property(ButtonSfx)
    public readonly buttonSfx?: ButtonSfx;

    private backgroundMusic?: BackgroundMusic;

    private soundState: SOUND_STATE = SOUND_STATE.SOUND_ON;

    constructor() {
        super('SpriteSound', ASSET_KEY.SPRITE_SOUND_ON);
    }

    start() {
        this.backgroundMusic = find('BackgroundMusic')?.getComponent(BackgroundMusic) as BackgroundMusic;
        const soundVolume = window.localStorage.getItem('Sound');

        if (soundVolume) {
            if (Number(soundVolume)) {
                this.turnOnSound();
            } else {
                this.turnOffSound();
            }
        } else {
            this.turnOnSound();
        }
    }

    switchSound() {
        if (this.soundState === SOUND_STATE.SOUND_ON) {
            this.turnOffSound();
            window.localStorage.setItem('Sound', String(0));
        } else {
            this.turnOnSound();
            window.localStorage.setItem('Sound', String(1));
        }
    }

    turnOnSound() {
        this.soundState = SOUND_STATE.SOUND_ON
        this.textureKey = ASSET_KEY.SPRITE_SOUND_ON;
        this.reload();
        this.backgroundMusic!.getComponent(AudioSource)!.volume = 0.5;
        if (this.buttonSfx) this.buttonSfx.volume = 1;
        if (this.eatSfx) this.eatSfx!.volume = 1;
        if (this.turnSfx) this.turnSfx!.volume = 1;
        if (this.crashSfx) this.crashSfx!.volume = 1;
    }

    turnOffSound() {
        this.soundState = SOUND_STATE.SOUND_OFF
        this.textureKey = ASSET_KEY.SPRITE_SOUND_OFF;
        this.reload();
        this.backgroundMusic!.getComponent(AudioSource)!.volume = 0;
        if (this.buttonSfx) this.buttonSfx!.volume = 0;
        if (this.eatSfx) this.eatSfx!.volume = 0;
        if (this.turnSfx) this.turnSfx!.volume = 0;
        if (this.crashSfx) this.crashSfx!.volume = 0;
    }
}