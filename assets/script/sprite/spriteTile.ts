
import { _decorator, tween, Color, Node, Sprite } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('SpriteTile')
export class SpriteTile extends BaseSprite {
    constructor(frame: number = 0) {
        super('SpriteTile', ASSET_KEY.SPRITE_TILE, frame);
    }

    turnIntoDarkTile() {
        this.frameKey = 1;
        this.reload();
    }

    turnIntoWallTile() {
        this.frameKey = undefined;
        this.textureKey = ASSET_KEY.SPRITE_WALL;
        this.reload();
    }
}