
import { _decorator, tween, Color, Node, Sprite } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('SpriteApple')
export class SpriteApple extends BaseSprite {
    constructor() {
        super('SpriteApple', ASSET_KEY.SPRITE_APPLE);
    }
}