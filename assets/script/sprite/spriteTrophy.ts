
import { _decorator, tween, Color, Node, Sprite } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('SpriteTrophy')
export class SpriteTrophy extends BaseSprite {
    constructor() {
        super('SpriteTrophy', ASSET_KEY.SPRITE_TROPHY);
    }
}