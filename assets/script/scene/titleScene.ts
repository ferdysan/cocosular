import { _decorator, Component, Node, director, game, AudioSource, Button } from 'cc';
import { BackgroundMusic } from '../audio/soundtrack/backgroundMusic';
import { SCENE_KEY } from '../enum/scene';
import { TRANSITION_SCREEN_EVENT } from '../enum/transitionScreen';
import { TransitionScreen } from '../sprite/transitionScreen';
const { ccclass, property } = _decorator;

@ccclass('TitleScene')
export class TitleScene extends Component {
    @property(BackgroundMusic)
    public readonly backgroundMusic?: BackgroundMusic;

    @property(TransitionScreen)
    public readonly transitionScreen?: TransitionScreen;

    @property(Button)
    public readonly playButton?: Button;

    onLoad() {
        if (this.backgroundMusic?.node) {
            game.addPersistRootNode(this.backgroundMusic!.node);
            this.backgroundMusic?.play();
        }
    }

    public goToGameScene() {
        this.playButton!.enabled = false;
        this.transitionScreen?.fadeIn(1);
        this.transitionScreen?.node.once(TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE, () => {
            director.loadScene(SCENE_KEY.GAME);
        });
    }
}
