import { _decorator, Component, Node, director, instantiate } from "cc";
import { levelConfigs } from "../config/level";
import { PlayerControl } from "../control/playerControl";
import { GAME_STATE } from "../enum/game";
import { SCENE_KEY } from "../enum/scene";
import { TRANSITION_SCREEN_EVENT } from "../enum/transitionScreen";
import { GameMap } from "../object/gameMap";
import { PopUpWindow } from "../object/popUpWindow";
import { Snake } from "../object/snake";
import { TransitionScreen } from "../sprite/transitionScreen";

const { ccclass, property } = _decorator;

@ccclass("GameScene")
export class GameScene extends Component {
  @property(GameMap)
  public readonly gameMap?: GameMap;

  @property(Snake)
  public readonly snake?: Snake;

  @property(PopUpWindow)
  public readonly popUpWindow?: PopUpWindow;

  @property(TransitionScreen)
  public readonly transitionScreen?: TransitionScreen;

  @property(PlayerControl)
  public readonly playerControl?: PlayerControl;

  private gameState?: GAME_STATE;

  private mapChoice = 0;

  constructor() {
    super("GameScene");
  }

  onLoad() {
    this.gameState = GAME_STATE.START;

    this.mapChoice = Math.floor(Math.random() * levelConfigs.length);

    const levelJSON = JSON.stringify(levelConfigs[this.mapChoice].snakeConfig);
    const level = JSON.parse(levelJSON);
    const mapJSON = JSON.stringify(levelConfigs[this.mapChoice].boardConfig);
    const map = JSON.parse(mapJSON);

    if (this.snake) {
      this.snake.SnakeConfig = level;
    }
    if (this.gameMap) {
      this.gameMap.BoardConfig = map;
    }

    this.gameMap?.createMap();
    if (this.snake?.isSnakeValid()) {
      this.snake?.createSnake();
      this.snake?.createApple();
    } else {
      const invalid = true;
      this.gameOverPopUp(invalid);
    }
  }

  start() {
    this.snake?.node.once(GAME_STATE.GAME_OVER, () => {
      this.gameOverPopUp();
    });

    this.popUpWindow?.node.once(GAME_STATE.RETRY, () => {
      if (this.gameState === GAME_STATE.GAME_OVER) {
        this.gameState = GAME_STATE.RETRY;
        this.goToGameScene();
      }
    });

    this.popUpWindow?.node.once(GAME_STATE.BACK_TO_MENU, () => {
      if (this.gameState === GAME_STATE.GAME_OVER) {
        this.gameState = GAME_STATE.BACK_TO_MENU;
        this.goToTitleScene();
      }
    });
  }

  gameOverPopUp(invalid = false) {
    this.playerControl?.disableControl();
    this.popUpWindow?.enableButtons();
    this.gameState = GAME_STATE.GAME_OVER;

    let node = this.popUpWindow!.node;

    node.active = true;
    this.popUpWindow?.enableButtons();

    if (invalid) {
      node.getComponent(PopUpWindow)?.showPopUpWindow("Snake Invalid", "");

      const posX = node
        .getComponent(PopUpWindow)
        ?.popUpText?.node?.getPosition().x!;
      const posY =
        node.getComponent(PopUpWindow)?.popUpText?.node?.getPosition().y! - 20;
      node.getComponent(PopUpWindow)?.popUpText?.node?.setPosition(posX, posY);
    } else {
      node.getComponent(PopUpWindow)?.showPopUpWindow();
    }
  }

  public goToGameScene() {
    this.popUpWindow?.disableButtons();

    this.transitionScreen?.fadeIn(1);
    this.transitionScreen?.node.once(
      TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE,
      () => {
        director.loadScene(SCENE_KEY.GAME);
      }
    );
  }

  public goToTitleScene() {
    this.popUpWindow?.disableButtons();

    this.transitionScreen?.fadeIn(1);
    this.transitionScreen?.node.once(
      TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE,
      () => {
        director.loadScene(SCENE_KEY.TITLE);
      }
    );
  }
}
