import { ASSET_EXTENSION, ASSET_KEY, ASSET_TYPE } from "../enum/asset";
import { AssetConfig } from "../interface/asset";

function getShopeeAssetUrl(url: string) {
  return `https://cf.shopee.co.id/file/${url}`;
}

export function getAssets() {
  const assets = new Array<AssetConfig>();

  // title
  assets.push({
    key: ASSET_KEY.LOGO_SHOPEE_ULAR,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/logo_shopee_ular",
  });

  // general UI
  assets.push({
    key: ASSET_KEY.TRANSITION_SCREEN,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/transitionScreen",
  });

  // menu UI
  assets.push({
    key: ASSET_KEY.SPRITE_SOUND_ON,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/sprite_sound_on",
  });
  assets.push({
    key: ASSET_KEY.SPRITE_SOUND_OFF,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/sprite_sound_off",
  });
  assets.push({
    key: ASSET_KEY.SPRITE_TROPHY,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/sprite_trophy",
  });

  // game UI
  assets.push({
    key: ASSET_KEY.KEYPAD,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/keypad",
  });

  // game sprites
  assets.push({
    key: ASSET_KEY.SPRITE_APPLE,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/sprite_apple",
  });
  assets.push({
    key: ASSET_KEY.SPRITE_TILE,
    type: ASSET_TYPE.SPRITESHEET,
    url: "",
    localUrl: "image/sprite_tile",
    config: {
      frameWidth: 48,
      frameHeight: 48,
    },
  });
  assets.push({
    key: ASSET_KEY.SPRITE_WALL,
    type: ASSET_TYPE.IMAGE,
    url: "",
    localUrl: "image/sprite_wall",
  });
  assets.push({
    key: ASSET_KEY.SPRITESHEET_ROUND_SNAKE,
    type: ASSET_TYPE.SPRITESHEET,
    url: "",
    localUrl: "image/spritesheet_round_snake",
    config: {
      frameWidth: 96.75,
      frameHeight: 96,
    },
  });
  assets.push({
    key: ASSET_KEY.SPRITESHEET_SNAKE,
    type: ASSET_TYPE.SPRITESHEET,
    url: "",
    localUrl: "image/spritesheet_snake",
    config: {
      frameWidth: 96.83,
      frameHeight: 96,
    },
  });

  // Soundtrack
  assets.push({
    key: ASSET_KEY.BG_MUSIC,
    type: ASSET_TYPE.AUDIO,
    url: "",
    localUrl: "audio/soundtrack/bg_music",
  });
  assets.push({
    key: ASSET_KEY.DONKEY_KONG_BG_MUSIC,
    type: ASSET_TYPE.AUDIO,
    url: "",
    localUrl: "audio/soundtrack/donkey_kong_bg_music",
  });

  // Misc
  assets.push({
    key: ASSET_KEY.SILENT_TRACK,
    type: ASSET_TYPE.AUDIO,
    url: "",
    localUrl: "audio/misc/silence",
  });

  // SFX
  assets.push({
    key: ASSET_KEY.BUTTON_SFX,
    type: ASSET_TYPE.AUDIO,
    url: "",
    localUrl: "audio/sfx/button_sfx",
  });
  assets.push({
    key: ASSET_KEY.CRASH,
    type: ASSET_TYPE.AUDIO,
    url: "",
    localUrl: "audio/sfx/crash",
  });
  assets.push({
    key: ASSET_KEY.EAT,
    type: ASSET_TYPE.AUDIO,
    url: "",
    localUrl: "audio/sfx/eat",
  });
  assets.push({
    key: ASSET_KEY.TURN,
    type: ASSET_TYPE.AUDIO,
    url: "",
    localUrl: "audio/sfx/turn",
  });

  return assets;
}
