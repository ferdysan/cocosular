import { _decorator, Component, Node, director, RichText } from 'cc';

const { ccclass, property } = _decorator;

@ccclass('ScoreManager')
export class ScoreManager extends Component {
    @property(RichText)
    public readonly scoreText?: RichText;

    @property(RichText)
    public readonly highscoreText?: RichText;

    private score?: number = 0;

    private highscore?: number = 0;

    get Score() {
        return this.score as number;
    }

    set Score(score: number) {
        this.score = score;
        this.scoreText!.string = String(score);
    }

    get Highscore() {
        return this.highscore as number;
    }

    set Highscore(highscore: number) {
        this.highscore = highscore;
        this.highscoreText!.string = String(highscore);
    }

    constructor() {
        super('ScoreManager');
    }

    onLoad() {
        this.loadHighScore();
    }

    loadHighScore() {
        this.Highscore = Number(localStorage.getItem('Highscore'));
    }

    saveScoreToHighscore() {
        if (this.score! >= this.highscore!) {
            localStorage.setItem('Highscore', String(this.score));
        }   
    }
}
