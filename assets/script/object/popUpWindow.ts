import {
  _decorator,
  Component,
  Node,
  director,
  RichText,
  Vec3,
  tween,
  Button,
} from "cc";
import { ButtonSfx } from "../audio/sfx/buttonSfx";
import { GAME_STATE } from "../enum/game";
import { ScoreManager } from "./scoreManager";

const { ccclass, property } = _decorator;

@ccclass("PopUpWindow")
export class PopUpWindow extends Component {
  @property(RichText)
  public readonly popUpText?: RichText;

  @property(RichText)
  public readonly popUpScore?: RichText;

  @property(Button)
  public readonly backToMenuButton?: Button;

  @property(Button)
  public readonly retryButton?: Button;

  @property(ScoreManager)
  public readonly scoreManager?: ScoreManager;

  @property(ButtonSfx)
  public readonly buttonSfx?: ButtonSfx;

  private isEnabledButtons = true;

  showPopUpWindow(
    popUpText = "Your Score",
    popUpScore = String(this.scoreManager?.Score)
  ) {
    this.popUpText!.string = popUpText;
    this.popUpScore!.string = popUpScore;
    this.node.scale = new Vec3(0.2, 0.2, 1);
    tween(this.node)
      .to(
        0.5,
        {
          scale: new Vec3(1, 1, 1),
        },
        {
          easing: "quartInOut",
        }
      )
      .start();
  }

  playButtonSound() {
    if (this.isEnabledButtons) {
      this.buttonSfx?.play();
    }
  }

  retryGame() {
    if (this.isEnabledButtons) {
      this.node.emit(GAME_STATE.RETRY);
    }
  }

  backToMenu() {
    if (this.isEnabledButtons) {
      this.node.emit(GAME_STATE.BACK_TO_MENU);
    }
  }

  enableButtons() {
    this.isEnabledButtons = true;
  }

  disableButtons() {
    this.isEnabledButtons = false;
  }
}
