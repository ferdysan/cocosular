import {
  _decorator,
  Component,
  Node,
  director,
  instantiate,
  tween,
  v3,
  Quat,
  Vec3,
  Color,
  Sprite,
} from "cc";
import { SpriteRoundSnake } from "../sprite/spriteRoundSnake";
import { BoardConfig, SnakeConfig, Position } from "../interfaces/gameMap";
import { GameMap } from "./gameMap";
import { manhattanDistance } from "../util/util";
import { PLAYER_CONTROL } from "../enum/control";
import { SpriteApple } from "../sprite/spriteApple";
import { ScoreManager } from "./scoreManager";
import { GAME_STATE } from "../enum/game";
import { EatSfx } from "../audio/sfx/eatSfx";
import { CrashSfx } from "../audio/sfx/crashSfx";
import { TurnSfx } from "../audio/sfx/turnSfx";

const { ccclass, property } = _decorator;

@ccclass("Snake")
export class Snake extends Component {
  @property(SpriteRoundSnake)
  public readonly spriteRoundSnake?: SpriteRoundSnake;

  @property(GameMap)
  public readonly gameMap?: GameMap;

  @property(SpriteApple)
  public readonly spriteApple?: SpriteApple;

  @property(ScoreManager)
  public readonly scoreManager?: ScoreManager;

  @property(EatSfx)
  public readonly eatSfx?: EatSfx;

  @property(CrashSfx)
  public readonly crashSfx?: CrashSfx;

  private apple?: SpriteApple;

  private appleConfig?: Position = { x: 0, y: 0 };

  private snakeBodyParts?: Array<SpriteRoundSnake> = [];

  private snakeConfig?: SnakeConfig;

  private initialDirection?: Array<number> = [];

  private moveQueue?: Array<Array<PLAYER_CONTROL>> = [];

  private eatingQueue?: Array<Array<boolean>> = [];

  private prevMove?: PLAYER_CONTROL;

  get SnakeBodyParts() {
    return this.snakeBodyParts;
  }

  get PrevMove() {
    return this.prevMove;
  }

  set SnakeConfig(snakeConfig: SnakeConfig) {
    this.snakeConfig = snakeConfig;
  }

  constructor() {
    super("Snake");
  }

  initializeSnakeMoves() {
    this.moveQueue!.push([]);
    this.eatingQueue!.push([]);

    for (let i = 1; i < (this.snakeConfig?.parts?.length as number); i++) {
      const dirX =
        this.snakeConfig!.parts[i - 1].x - this.snakeConfig!.parts[i].x;
      const dirY =
        this.snakeConfig!.parts[i - 1].y - this.snakeConfig!.parts[i].y;
      let moveDir = PLAYER_CONTROL.UP;

      if (dirX === -1) {
        // LEFT
        moveDir = PLAYER_CONTROL.LEFT;
        this.initialDirection!.push(90);
      } else if (dirX === 1) {
        // RIGHT
        moveDir = PLAYER_CONTROL.RIGHT;
        this.initialDirection!.push(270);
      } else if (dirY === -1) {
        // UP
        moveDir = PLAYER_CONTROL.UP;
        this.initialDirection!.push(0);
      } else if (dirY === 1) {
        // DOWN
        moveDir = PLAYER_CONTROL.DOWN;
        this.initialDirection!.push(180);
      }

      let tempMoveQueue = Object.assign([], this.moveQueue![i - 1]);
      tempMoveQueue.unshift(moveDir);

      let tempEatingQueue = Object.assign([], this.eatingQueue![i - 1]);
      tempEatingQueue.unshift(false);

      this.moveQueue!.push(tempMoveQueue);
      this.eatingQueue!.push(tempEatingQueue);
    }

    // Assign direction for snake head
    this.initialDirection?.unshift(this.initialDirection[0]);
  }

  isOccupyWall(snakePart: Position) {
    if (this.gameMap?.BoardConfig.tiles[snakePart.y][snakePart.x] === 1) {
      return true;
    }

    return false;
  }

  moveScheduler() {
    if (this.moveQueue![0].length === 0) {
      this.addMove(this.prevMove as PLAYER_CONTROL);
    }

    this.moveSnake();
  }

  isMoveValid(move: PLAYER_CONTROL) {
    // Snake head cannot move in the opposite direction of its current movement
    if (move === PLAYER_CONTROL.UP && this.prevMove === PLAYER_CONTROL.DOWN)
      return false;

    if (move === PLAYER_CONTROL.DOWN && this.prevMove === PLAYER_CONTROL.UP)
      return false;

    if (move === PLAYER_CONTROL.LEFT && this.prevMove === PLAYER_CONTROL.RIGHT)
      return false;

    if (move === PLAYER_CONTROL.RIGHT && this.prevMove === PLAYER_CONTROL.LEFT)
      return false;

    return true;
  }

  isHitSnake(x: number, y: number, checkHead = false) {
    // Check if there's a snake part in the selected tile
    let start = 1;

    if (checkHead) {
      start = 0;
    }

    for (let i = start; i < this.snakeConfig!.parts.length; i++) {
      if (
        this.snakeConfig!.parts[i].x === x &&
        this.snakeConfig!.parts[i].y === y
      )
        return true;

      // Check if apple is between snake head and body
      if (
        i === 0 &&
        manhattanDistance(
          x,
          y,
          this.snakeConfig!.parts[i].x,
          this.snakeConfig!.parts[i].y
        ) === 1
      )
        return true;
    }

    return false;
  }

  isHitWall(x: number, y: number) {
    // Check if edge of map
    if (
      x < 0 ||
      y < 0 ||
      y >= this.gameMap!.BoardConfig.tiles.length ||
      x >= this.gameMap!.BoardConfig.tiles[0].length
    )
      return true;

    // Check if wall
    if (this.gameMap?.BoardConfig.tiles[y][x] === 1) return true;

    return false;
  }

  isHitApple(x: number, y: number) {
    // Check if there's an apple in the selected tile
    if (this.appleConfig!.x === x && this.appleConfig!.y === y) return true;

    return false;
  }

  eatApple() {
    this.scoreManager!.Score += 1;
    if (this.scoreManager!.Score > this.scoreManager!.Highscore) {
      this.scoreManager!.Highscore = this.scoreManager!.Score;
    }

    this.eatSfx?.play();
    this.apple?.node.destroy();

    // Spawn next apple
    this.createApple();
  }

  createApple() {
    const { gameMap, spriteApple } = this;
    if (!spriteApple || !gameMap) return;

    let isValid = false;
    let randomX = 0;
    let randomY = 0;

    while (!isValid) {
      randomX = Math.floor(
        Math.random() * gameMap!.BoardConfig.tiles[0].length
      );
      randomY = Math.floor(Math.random() * gameMap!.BoardConfig.tiles.length);

      if (
        !this.isHitWall(randomX, randomY) &&
        !this.isHitSnake(randomX, randomY, true)
      ) {
        isValid = true;
      }
    }

    let node = instantiate(spriteApple?.node);

    node.parent = this.node;

    const applePosition =
      gameMap.spriteTiles[randomY][randomX]?.node?.getPosition();
    node.setPosition(applePosition);

    // Push Apple to Apple Object
    this.apple = node?.getComponent(SpriteApple) as SpriteApple;
    this.appleConfig!.x = randomX;
    this.appleConfig!.y = randomY;
  }

  isSnakeValid() {
    const { spriteRoundSnake } = this;
    if (!spriteRoundSnake) return false;

    // Check snake length
    if ((this.snakeConfig?.parts?.length as number) < 3) return false;

    // Check the first Element
    if (this.isOccupyWall(this.snakeConfig?.parts[0] as Position)) return false;

    // Check the rest of the Elements
    for (let i = 1; i < (this.snakeConfig?.parts?.length as number); i++) {
      // Check occupy wall
      if (this.isOccupyWall(this.snakeConfig?.parts[i] as Position))
        return false;

      // Check manhattan distance with previous snakePart
      if (
        manhattanDistance(
          this.snakeConfig?.parts[i].x as number,
          this.snakeConfig?.parts[i].y as number,
          this.snakeConfig?.parts[i - 1].x as number,
          this.snakeConfig?.parts[i - 1].y as number
        ) !== 1
      )
        return false;
    }

    return true;
  }

  createSnake() {
    const { spriteRoundSnake, gameMap } = this;
    if (!spriteRoundSnake || !gameMap) return;

    this.initializeSnakeMoves();

    this.snakeConfig?.parts?.forEach((part, i) => {
      let node = instantiate(spriteRoundSnake?.node);

      node.parent = this.node;

      const posX = part.x;
      const posY = part.y;

      const snakePosition =
        gameMap.spriteTiles[posY][posX]?.node?.getPosition();
      node.setPosition(snakePosition);
      node.angle = this.initialDirection![i];

      // Turn into Body
      if (i > 0 && i < (this.snakeConfig?.parts?.length as number) - 1) {
        node?.getComponent(SpriteRoundSnake)?.turnIntoBody();
      }

      // Turn into Tail
      if (i === (this.snakeConfig?.parts?.length as number) - 1) {
        node?.getComponent(SpriteRoundSnake)?.turnIntoTail();
      }

      // Push Snake Parts to Snake Object
      this.snakeBodyParts?.push(
        node?.getComponent(SpriteRoundSnake) as SpriteRoundSnake
      );
    });
  }

  addTail() {
    const lastIdx = this.snakeBodyParts!.length - 1;
    const tail = Object.assign({}, this.snakeBodyParts![lastIdx]);
    const part = Object.assign({}, this.snakeConfig?.parts[lastIdx]!);
    let node = instantiate(this.spriteRoundSnake?.node)!;
    node!.getComponent(Sprite)!.color = new Color(11, 255, 77);

    node.parent = this.node;

    const posX = part.x;
    const posY = part.y;

    const snakePosition =
      this.gameMap?.spriteTiles[posY][posX]?.node?.getPosition();
    node.setPosition(snakePosition as Vec3);
    node.angle = tail.node.angle;

    // turn current tail to body
    this.snakeBodyParts![lastIdx]?.turnIntoBody();
    this.snakeBodyParts![lastIdx].node!.getComponent(Sprite)!.color = new Color(
      11,
      255,
      77
    );

    // Push Snake Parts to Snake Object
    this.snakeBodyParts?.push(
      node?.getComponent(SpriteRoundSnake) as SpriteRoundSnake
    );

    // Add new snake config
    this.snakeConfig?.parts.push(part);

    // turn new part to tail
    node?.getComponent(SpriteRoundSnake)?.turnIntoBodyEating();

    // add new Move Queue
    let newMove = Object.assign([], this.moveQueue![lastIdx]);
    newMove.unshift(Object.assign({}, this.moveQueue![lastIdx][0]));

    // add new eating queue
    let newEat = Object.assign([], this.eatingQueue![lastIdx]);
    newEat.unshift(this.eatingQueue![lastIdx][0]);

    this.moveQueue?.push(newMove);
    this.eatingQueue?.push(newEat);
  }

  addMove(move: PLAYER_CONTROL) {
    if (!this.prevMove) {
      this.prevMove = move;
      this.schedule(this.moveScheduler, this.snakeConfig?.interval?.initial);
    } else {
      // Check if move is valid on snake head
      if (this.isMoveValid(move)) {
        this.moveQueue?.forEach((queue, i) => {
          queue.push(move);
          this.eatingQueue![i].push(false);
        });
      }
    }
  }

  moveSnake() {
    // Move each snake part by one move (head of queue)

    // Whether the snake is currently eating an Apple or not
    let isEating = false;

    for (let i = 0; i < this.snakeBodyParts!.length; i++) {
      this.snakeBodyParts![i].node!.getComponent(Sprite)!.color = new Color(
        255,
        255,
        255
      );
      let nextMove = this.moveQueue![i].shift();
      let nextEat = this.eatingQueue![i].shift();

      if (i === 0) {
        this.prevMove = nextMove;
      }

      let angle = 0;

      switch (nextMove) {
        case PLAYER_CONTROL.UP: {
          this.snakeConfig!.parts[i].y -= 1;
          angle = 0;
          break;
        }
        case PLAYER_CONTROL.DOWN: {
          this.snakeConfig!.parts[i].y += 1;
          angle = 180;
          break;
        }
        case PLAYER_CONTROL.LEFT: {
          this.snakeConfig!.parts[i].x -= 1;
          angle = 90;
          break;
        }
        case PLAYER_CONTROL.RIGHT: {
          this.snakeConfig!.parts[i].x += 1;
          angle = 270;
          break;
        }
        default:
          break;
      }

      const posX = this.snakeConfig!.parts[i].x;
      const posY = this.snakeConfig!.parts[i].y;

      // Check if head hits wall or self cannibalism
      if (
        i === 0 &&
        (this.isHitWall(posX, posY) || this.isHitSnake(posX, posY, false))
      ) {
        // Game Over
        this.scoreManager?.saveScoreToHighscore();
        this.unschedule(this.moveScheduler);
        this.crashSfx?.play();
        setTimeout(() => {
          this.node.emit(GAME_STATE.GAME_OVER);
        }, this.snakeConfig!.interval.initial);

        return;
      }

      // Check if head hits apple
      if (i === 0 && this.isHitApple(posX, posY)) {
        this.eatApple();
        if (
          this.scoreManager!.Score %
            this.snakeConfig!.interval.accelerateEvery ===
          0
        ) {
          const newInterval =
            this.snakeConfig!.interval.initial *
            this.snakeConfig!.interval.accelerateMultiplier;
          if (newInterval >= this.snakeConfig!.interval.minimum) {
            this.snakeConfig!.interval.initial = newInterval;
            this.unschedule(this.moveScheduler);
            this.schedule(this.moveScheduler, newInterval);
          }
        }
        isEating = true;
      }

      if (i > 0 && i < this.snakeBodyParts!.length && isEating) {
        this.eatingQueue![i][this.eatingQueue![i].length - 1] = true;
      }

      this.snakeBodyParts![i].node.angle = angle;

      if (nextEat && i < this.snakeBodyParts!.length) {
        this.snakeBodyParts![i].node!.getComponent(Sprite)!.color = new Color(
          11,
          255,
          77
        );
        tween(this.snakeBodyParts![i].node)
          .to(this.snakeConfig?.interval?.initial as number, {
            scale: new Vec3(2, 1.5, 0),
          })
          .to(this.snakeConfig?.interval?.initial as number, {
            scale: new Vec3(1, 1, 0),
          })
          .start();
      }

      tween(this.snakeBodyParts![i].node)
        .to(this.snakeConfig?.interval?.initial as number, {
          position: v3(
            this.gameMap?.spriteTiles[posY][posX]?.node.getPosition() as Vec3
          ),
        })
        .call(() => {
          if (
            i === this.snakeBodyParts!.length - 1 &&
            this.snakeBodyParts![i].FrameKey !== 2
          ) {
            // turn current tail to tail sprite
            this.snakeBodyParts![i]?.turnIntoTail();
          }
          if (nextEat && i === this.snakeBodyParts!.length - 1) this.addTail();
        })
        .start();
    }
  }
}
