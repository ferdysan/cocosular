import {
  _decorator,
  Component,
  Node,
  director,
  v2,
  instantiate,
  Sprite,
  Vec3,
  Prefab,
} from "cc";
import { SpriteTile } from "../sprite/spriteTile";
import { levelConfigs } from "../config/level";
import { BoardConfig, SnakeConfig } from "../interfaces/gameMap";

const { ccclass, property } = _decorator;

@ccclass("GameMap")
export class GameMap extends Component {
  @property(SpriteTile)
  public readonly spriteTile?: SpriteTile;

  public spriteTiles: Array<Array<SpriteTile>> = [];

  private boardConfig?: BoardConfig;

  get BoardConfig() {
    return this.boardConfig as BoardConfig;
  }

  set BoardConfig(boardConfig: BoardConfig) {
    this.boardConfig = boardConfig;
  }

  constructor() {
    super("GameMap");

    const mapChoice = 0;

    this.boardConfig = levelConfigs[mapChoice].boardConfig;
  }

  createMap() {
    let tileX = -148;
    let tileY = 162;

    this.boardConfig?.tiles?.forEach((tileRow, i) => {
      let rowTiles: Array<SpriteTile> = [];
      tileRow.forEach((tile, j) => {
        const node = this.createTile(tileX, tileY);

        if (tile === 0) {
          // turn into dark tile for alternating tiles
          if ((i % 2 === 0 && j % 2 === 1) || (i % 2 === 1 && j % 2 === 0)) {
            node?.getComponent(SpriteTile)?.turnIntoDarkTile();
          }
        } else if (tile === 1) {
          // turn into wall tile
          node?.getComponent(SpriteTile)?.turnIntoWallTile();
        }

        rowTiles.push(node?.getComponent(SpriteTile) as SpriteTile);
        tileX += 27;
      });

      this.spriteTiles.push(rowTiles);
      tileX = -148;
      tileY -= 27;
    });
  }

  createTile(x: number, y: number) {
    const { spriteTile } = this;
    if (!spriteTile) return;

    let node = instantiate(spriteTile?.node);

    node.parent = this.node;
    node.setPosition(x, y);

    return node;
  }
}
