import { _decorator, Component, Node, RichText, ProgressBar } from 'cc';
import { LogoShopeeUlar } from './sprite/logoShopeeUlar';
const { ccclass, property } = _decorator;

@ccclass('AssetLoadingUI')
export class AssetLoadingUI extends Component {
    @property(LogoShopeeUlar)
    public logoShopeeUlar?: LogoShopeeUlar;

    @property(RichText)
    public percentLoadText?: RichText;

    @property(RichText)
    public urlLoadText?: RichText;

    @property(ProgressBar)
    public progressBar?: ProgressBar;

    public updateText(progress: number, key?: string) {
        const { percentLoadText, urlLoadText } = this;
        const progressPercent = Math.floor(progress * 100);

        this.logoShopeeUlar?.reload();

        if (percentLoadText) {
            percentLoadText.string = `${progressPercent}%`;

            if (this.progressBar) {
                this.progressBar.progress = progressPercent / 100;
            }
        }
        
        if (urlLoadText) {
            switch(progressPercent) {
                case 100: {
                    urlLoadText.string = 'Click To Enter';
                    break;
                }

                case 0: {
                    urlLoadText.string = 'Loading...';
                    break;
                }

                default: {
                    urlLoadText.string = `${key}`;
                    break;
                }
            }
        }
    }
}
