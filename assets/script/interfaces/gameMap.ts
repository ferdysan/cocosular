// boardConfig: Array[13][12] // -> playing field, 0 is tile, 1 is wall
// snakeConfig: {
//     parts: Array<{x: number, y: number}> // -> Indicate the indexes of each snake part on the board
//     interval: {
//         initial: number; // -> The initial update interval, 0.3 means snake moves every 0.3 seconds
//         accelerateMultiplier: number; // -> The acceleration, 0.9 means multiply interval by 0.9 every time it accelerates (e.g. 0.3s becomes 0.3s * 0.9 = 0.27s after first acceleration)
//         accelerateEvery: number; // -> How many fruits the snake have to eat before it accelerates
//         minimum: number; // -> Minimum interval (a.k.a speed cap), update interval can't accelerate below this number
//     }
// }

export interface BoardConfig {
    tiles: Array<Array<number>>;
}

export interface Position {
    x: number;
    y: number;
}

export interface SnakeConfig {
    parts: Array<Position>;
    interval: {
        initial: number;
        accelerateMultiplier: number;
        accelerateEvery: number;
        minimum: number;
    }
}