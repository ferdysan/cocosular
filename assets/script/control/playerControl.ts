import { _decorator, Component, Node, systemEvent, SystemEvent } from 'cc';
import { TurnSfx } from '../audio/sfx/turnSfx';
import { PLAYER_CONTROL } from '../enum/control';
import { Snake } from '../object/snake';

const { ccclass, property } = _decorator;

@ccclass('PlayerControl')
export class PlayerControl extends Component {
    @property(Snake)
    public readonly snake?: Snake;

    @property(TurnSfx)
    public readonly turnSfx?: TurnSfx;

    private isEnabledControl = true;

    public up() {
        if (this.snake?.SnakeBodyParts && this.snake?.SnakeBodyParts?.length !== 0 && this.isEnabledControl) {
            if (this.snake?.PrevMove !== PLAYER_CONTROL.UP && this.snake?.SnakeBodyParts![0].node.angle !== 180) {
                this.turnSfx?.play();
                this.snake?.addMove(PLAYER_CONTROL.UP);
            }
        }
    }

    public down() {
        if (this.snake?.SnakeBodyParts && this.snake?.SnakeBodyParts?.length !== 0 && this.isEnabledControl) {
            if (this.snake?.PrevMove !== PLAYER_CONTROL.DOWN && this.snake?.SnakeBodyParts![0].node.angle !== 0) {
                this.turnSfx?.play();
                this.snake?.addMove(PLAYER_CONTROL.DOWN);
            }
        }
    }

    public left() {
        if (this.snake?.SnakeBodyParts && this.snake?.SnakeBodyParts?.length !== 0 && this.isEnabledControl) {
            if (this.snake?.PrevMove !== PLAYER_CONTROL.LEFT && this.snake?.SnakeBodyParts![0].node.angle !== 270) {
                this.turnSfx?.play();
                this.snake?.addMove(PLAYER_CONTROL.LEFT);
            }
        }
    }

    public right() {
        if (this.snake?.SnakeBodyParts && this.snake?.SnakeBodyParts?.length !== 0 && this.isEnabledControl) {
            if (this.snake?.PrevMove !== PLAYER_CONTROL.RIGHT && this.snake?.SnakeBodyParts![0].node.angle !== 90) {
                this.turnSfx?.play();
                this.snake?.addMove(PLAYER_CONTROL.RIGHT);
            }
        }
    }

    onLoad() {
        systemEvent.on(SystemEvent.EventType.KEY_DOWN, (event) => {
            switch(event.keyCode) {
                case 38: {
                    //up
                    this.up();
                    break;
                }

                case 40: {
                    //down
                    this.down();
                    break;
                }

                case 37: {
                    //left
                    this.left();
                    break;
                }

                case 39: {
                    //right
                    this.right();
                    break;
                }

                default: {
                    break;
                }
            }
        });
    }

    disableControl() {
        this.isEnabledControl = false;
    }
}