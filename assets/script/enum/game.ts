export enum GAME_STATE {
    START = 'start',
    GAME_OVER = 'game_over',
    RETRY = 'retry',
    BACK_TO_MENU = 'back_to_menu',
}