export enum ASSET_EXTENSION {
  PNG = '.png',
}

export enum ASSET_TYPE {
  IMAGE = 'image',
  SPRITESHEET = 'spritesheet',
  AUDIO = 'audio',
}

export enum ASSET_KEY {

  TRANSITION_SCREEN = 'transition_screen',

  // title UI
  LOGO_SHOPEE_ULAR = 'logo_shopee_ular',

  // menu UI
  SPRITE_SOUND_ON = 'sprite_sound_on',
  SPRITE_SOUND_OFF = 'sprite_sound_off',
  SPRITE_TROPHY = 'sprite_trophy',

  // game UI
  KEYPAD = 'keypad',

  // game sprites
  SPRITE_APPLE = 'sprite_apple',
  SPRITE_TILE = 'sprite_tile',
  SPRITE_WALL = 'sprite_wall',
  SPRITESHEET_ROUND_SNAKE = 'spritesheet_round_snake',
  SPRITESHEET_SNAKE = 'spritesheet_snake',

  // soundtrack
  BG_MUSIC = 'bg_music',
  DONKEY_KONG_BG_MUSIC = 'donkey_kong_bg_music',

  // Misc
  SILENT_TRACK = 'silence',

  // SFX
  BUTTON_SFX = 'button_sfx',
  CRASH = 'crash',
  EAT = 'eat',
  TURN = 'turn',
}